var axios = require("axios");
var config = require("../config/config");
var logFile = require("../helper/logger");
var config = require("../config/config");
var jwt = require("jsonwebtoken");
var _ = require("lodash");

class NohasaCommon {

    constructor() {}

    getUserInformation(username) {
        return axios.get(config.application.API_URL + `getUserInfoByUserName?username=${username}`, {
                headers: {
                    Authorization: token
                }
            })
            .then(response => {
                return response.data.data[0];
            })
            .catch(function (error) {
                logFile.error("getUserInformation: " + error);
            });
    }

    renderView(req, res, token, view, title) {

        var decodedToken;

        try {
            decodedToken = jwt.verify(token, config.application.SECRET_KEY);
        } catch (error) {
            res.redirect("/");
            logFile.error("Token decoded: " + error);
        }

        this.getUserInformation(decodedToken.username).then(data => {
            res.render(`screens/${view}`, {
                title: `${title}`,
                username: decodedToken.username,
                name: _.toUpper(decodedToken.scope.name),
                phone: data.phone
            });
        });
    }
}

module.exports = new NohasaCommon();