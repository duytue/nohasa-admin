var axios = require("axios");
var config = require("../config/config");
var logFile = require("../helper/logger");

exports.get_homepage = function (req, res) {
  res.clearCookie("token")
  res.render("screens/index", {
    title: "Login"
  });
};

exports.login = function (req, res) {

  axios.post(config.application.API_URL + "login", {
      username: req.body.username,
      password: req.body.password
    })
    .then(function (response) {
      res.cookie("token", response.data.access_token);
      res.send({
        isAuthenticate: true
      })
    })
    .catch(function (error) {
      logFile.error("login: " + error)
      res.send({
        isAuthenticate: false
      });
    });
}

exports.logout = function (req, res) {
  res.clearCookie("token");
  res.redirect("/");
};