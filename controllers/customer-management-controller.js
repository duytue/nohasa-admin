var NohasaCommon = require("./common");

exports.get_customer_management = function (req, res) {

    token = req.cookies.token;
    view = "customer-management/customer-management";
    title = "Customer Management";

    NohasaCommon.renderView(req, res, token, view, title);

};