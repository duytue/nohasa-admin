var axios = require("axios");
var config = require("../config/config");
var jwt = require("jsonwebtoken");
var _ = require("lodash");
var logFile = require("../helper/logger");
var NohasaCommon = require("./common")

function getProductsForAdmin() {
    return axios.get(config.application.API_URL + `getProductsForAdmin`)
        .then(response => {
            return response.data.data;
        })
        .catch(function (error) {
            logFile.error("getProductsForAdmin: " + error);
        });
}

function updateProduct(req, res) {
    return axios.put(config.application.API_URL + `updateProduct`, {
            productId: req.body.productId,
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            altName: req.body.altName,
            isDeleted: req.body.isDeleted
        })
        .then(response => {
            return response.data.data;
        })
        .catch(function (error) {
            logFile.error("updateProduct: " + error);
        });
}

function getDevelopmentModeInfo() {
    return axios.get(config.application.CLOUDFLARE_API_URL + "/settings/development_mode", {
            headers: {
                "X-Auth-Key": config.application.X_AUTH_KEY,
                "X-Auth-Email": config.application.X_AUTH_EMAIL
            }
        })
        .then(response => {
            return response.data;
        })
        .catch(function (error) {
            logFile.error("getDevelopmentModeInfo: " + error);
        });
}

function changeDevelopmentMode(req, res) {
    return axios.patch(config.application.CLOUDFLARE_API_URL + "/settings/development_mode", {
            value: req.body.data
        }, {
            headers: {
                "X-Auth-Key": config.application.X_AUTH_KEY,
                "X-Auth-Email": config.application.X_AUTH_EMAIL
            }
        })
        .then(response => {
            return response.data;
        })
        .catch(function (error) {
            logFile.error("changeDevelopmentMode: " + error);
        });
}

function purgeCache() {
    return axios.post(config.application.CLOUDFLARE_API_URL + "/purge_cache", {
            purge_everything: true
        }, {
            headers: {
                "X-Auth-Key": config.application.X_AUTH_KEY,
                "X-Auth-Email": config.application.X_AUTH_EMAIL
            }
        })
        .then(response => {
            return response.data;
        })
        .catch(function (error) {
            logFile.error("changeDevelopmentMode: " + error);
        });
}

exports.get_dashboard = function (req, res) {

    token = req.cookies.token;
    view = "dashboard/dashboard";
    title = "Dashboard";

    NohasaCommon.renderView(req, res, token, view, title);
};

exports.add_product = function (req, res) {
    axios.post(config.application.API_URL + "addProduct", {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            altName: req.body.altName
        }, {
            headers: {
                Authorization: req.cookies.token
            }
        })
        .then(response => {
            res.send({
                isSucess: true,
                data: response.data
            });
            logFile.info("addProduct: " + response);
        })
        .catch(function (error) {
            res.statusCode = 500;
            res.send({
                isSuccess: false,
                message: "Unable to add product"
            });
            logFile.error("addProduct: " + error);
        });
};

exports.upload_image = function (req, res, next) {
    if (req.files === undefined) {
        res.statusCode = 500;
        res.send({
            isSuccess: false,
            message: "Failed to upload image(s)"
        });
    }
    // if upload success
    res.send({
        isSuccess: true,
        message: "Image(s) upload successfully",
        data: req.files
    });
};

exports.add_image_to_product = function (req, res, next) {
    const s3Url = config.application.S3_URL;
    axios.post(config.application.API_URL + "addImageToProduct", {
            imageURL: s3Url + req.body.imageURL,
            uploadName: req.body.uploadName,
            productId: req.body.productId
        }, {
            headers: {
                Authorization: req.cookies.token
            }
        })
        .then(response => {
            res.send({
                isSucess: true,
                data: response.data
            });
            logFile.info("add_image_to_product: " + response);
        })
        .catch(function (error) {
            res.statusCode = 500;
            res.send({
                isSuccess: false,
                message: "Unable to add image(s) to product"
            });
            logFile.error("add_image_to_product: " + error);
        });
};

exports.get_product_images = function (req, res, next) {
    axios.get(config.application.API_URL + `getProductImageById?productId=${req.query.productId}`, {
            headers: {
                Authorization: req.cookies.token
            }
        })
        .then(response => {
            res.send({
                data: response.data.data
            });
        })
        .catch(function (error) {
            res.statusCode = 500;
            res.send({
                message: "Unable to get product image(s)"
            });
            logFile.error("getProductImage: " + error);
        });
};

exports.update_default_image = function (req, res) {
    axios.post(config.application.API_URL + "updateProductDefaultImage", {
            productId: req.body.productId,
            isHomePageImg: req.body.isHomePageImg,
            imageId: req.body.imageId
        }, {
            headers: {
                Authorization: req.cookies.token
            }
        })
        .then(response => {
            res.send({
                isSucess: true,
                data: response.data
            });
            logFile.info("updateProductDefaultImage: " + response);
        })
        .catch(function (error) {
            res.statusCode = 500;
            res.send({
                isSuccess: false,
                message: "Unable to update default image for product"
            });
            logFile.error("updateProductDefaultImage: " + error);
        });
};

exports.get_list_product = function (req, res) {
    getProductsForAdmin(req).then(data => {
        res.send(data);
    });
};

exports.update_product = function (req, res) {
    updateProduct(req).then(data => {
        res.send(data);
    });
};

exports.delete_product = function (req, res) {
    axios.get(config.application.API_URL + `deleteProduct?productId=${req.query.productId}`)
        .then(response => {
            res.send({
                isSuccess: true
            });
        })
        .catch(function (error) {
            res.statusCode = 500;
            res.send({
                message: "Unable to delete product"
            });
            logFile.error("deleteProduct: " + error);
        });
};

exports.get_development_mode_info = function (req, res) {
    getDevelopmentModeInfo(req).then(data => {
        res.send(data);
    });
};

exports.change_development_mode = function (req, res) {
    changeDevelopmentMode(req).then(data => {
        res.send(data);
    });
};

exports.purge_cache = function (req, res) {
    purgeCache(req).then(data => {
        res.send(data);
    });
};