#!/bin/bash

# any future command that fails will exit the script
set -e

# cd to repo
cd /home/ubuntu/nohasa-admin

# clone the repo again
echo "Pulling source code"
git pull origin master
echo "Source code pulled successfully"

#install npm packages
echo "Running npm install"
npm install
echo "npm install done"

#Restart the node server
echo "Stop server"
pm2 delete admin
echo "Server stopped"
echo "Restarting server"
npm start
echo "Restart server successfully"

pm2 list
echo "Deploy done"