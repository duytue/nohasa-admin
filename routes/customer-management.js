var express = require("express");
var router = express.Router();
var customerManagementController = require("../controllers/customer-management-controller");

router.get("/customer-management", customerManagementController.get_customer_management);

module.exports = router;