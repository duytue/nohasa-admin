var express = require("express");
var router = express.Router();
var indexController = require("../controllers/index-controller");

router.get("/", indexController.get_homepage);

router.get("/logout", indexController.logout);

router.post("/login", indexController.login);

module.exports = router;