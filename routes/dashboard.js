var express = require("express");
var router = express.Router();
var dashboardController = require("../controllers/dashboard-controller");
var multer = require("multer");
var multerS3 = require("multer-s3");
var config = require("../config/config");
var aws = require("aws-sdk");

aws.config.update({
  secretAccessKey: config.application.IAM_USER_SECRET,
  accessKeyId: config.application.IAM_USER_KEY,
  region: "us-east-1"
});

var s3 = new aws.S3();
var uploadS3 = multer({
  storage: multerS3({
    s3: s3,
    bucket: config.application.BUCKET_NAME,
    acl: "public-read",
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, file.originalname);
    }
  })
});

router.get("/dashboard", dashboardController.get_dashboard);

router.post("/addProduct", dashboardController.add_product);

router.post("/addImageToProduct", dashboardController.add_image_to_product);

router.post("/uploadImage", uploadS3.array("imgUpload", config.application.MAX_NUMBER_FILE_UPLOAD), dashboardController.upload_image);

router.get("/getProductImages", dashboardController.get_product_images);

router.post("/updateProductDefaultImage", dashboardController.update_default_image);

router.get("/getProducts", dashboardController.get_list_product);

router.get("/deleteProduct", dashboardController.delete_product);

router.put("/updateProduct", dashboardController.update_product);

router.get("/getDevelopmentModeInfo", dashboardController.get_development_mode_info);

router.patch("/changeDevelopmentMode", dashboardController.change_development_mode);

router.post("/purgeCache", dashboardController.purge_cache);

module.exports = router;