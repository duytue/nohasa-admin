$("#product-grid").jsGrid({
    width: "100%",
    height: "auto",

    autoload: true,
    editing: true,
    sorting: true,
    paging: true,

    deleteConfirm: function (item) {
        return "Item \"" + item.name + "\" will be removed. Are you sure?";
    },

    controller: {
        loadData: function (filter) {
            return $.ajax({
                method: "GET",
                url: "/getProducts"
            });
        },

        updateItem: function (item) {
            return $.ajax({
                type: "PUT",
                url: "/updateProduct",
                data: {
                    productId: item.id,
                    name: item.name,
                    description: item.description,
                    price: item.price,
                    altName: item.altName,
                    isDeleted: item.isDeleted
                }
            }).done(
                reloadGrid(),
                M.toast({
                    html: "Updated successfully !"
                }));
        },

        deleteItem: function (item) {
            return $.ajax({
                type: "GET",
                url: "/deleteProduct",
                data: {
                    productId: item.id
                },
            }).done(
                reloadGrid(),
                M.toast({
                    html: "Deleted successfully !"
                }));
        }
    },

    fields: [{
            name: "name",
            title: "Product Name",
            type: "text",
            width: 50,
            validate: "required"
        },
        {
            name: "description",
            title: "Product Description",
            type: "text",
            width: 250,
            validate: "required"
        },
        {
            name: "price",
            title: "Price",
            type: "number",
            width: 50,
            validate: "required"
        },
        {
            name: "altName",
            type: "text",
            width: 50,
            validate: "required"
        },
        {
            name: "isDeleted",
            width: 50,
            type: "number",
            title: "Is Deleted"
        },
        {
            title: "Action",
            type: "control"
        }
    ]
});