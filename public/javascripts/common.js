// dropdown nav bar
$(document).ready(function () {
  $("#user-dropdown").dropdown({
    hover: true,
    alignment: "bottom",
    coverTrigger: false
  });
})

// Modal
document.addEventListener("DOMContentLoaded", function () {
  var elems = document.querySelectorAll(".modal");
  var modal = M.Modal.init(elems, 0.5);
});

// datetime picker
$(document).ready(function () {
  $('.datepicker').datepicker({
    selectMonths: true, // Creates a dropdown to control month
    yearRange: [1970, (new Date()).getFullYear()], // Creates a dropdown of 15 years to control year
    format: "yyyy-mm-dd"
  });
});

// drop-down
document.addEventListener("DOMContentLoaded", function () {
  var elems = document.querySelectorAll("select");
  var dropdown = M.FormSelect.init(elems, {});
});

// Carousel
document.addEventListener("DOMContentLoaded", function () {
  var elems = document.querySelectorAll(".carousel");
  var carousel = M.Carousel.init(elems, {
    fullWidth: true,
    indicators: true
  });
});

// clear modal after closed
function clearModal() {
  $(document).ready(function () {
    $('#regis-modal').find('input:text, input:password, select, textarea').val('');
    $('#regis-modal').find('input:checkbox').prop('checked', false);
    $("#email").val("");
    document.getElementById("fn").innerHTML = "";
    document.getElementById("ln").innerHTML = "";
    document.getElementById("em").innerHTML = "";
    document.getElementById("pn").innerHTML = "";
    document.getElementById("un").innerHTML = "";
    document.getElementById("pw").innerHTML = "";
    document.getElementById("rpw").innerHTML = "";
    document.getElementById("bd").innerHTML = "";
    document.getElementById("sc").innerHTML = "";
  });
}

// init tootip
document.addEventListener('DOMContentLoaded', function () {
  var bookingTootips = document.querySelectorAll('.tooltipped');
  var bookingTootipInstance = M.Tooltip.init(bookingTootips, {});
});

// init timepicker
document.addEventListener('DOMContentLoaded', function () {
  var timePicker = document.querySelectorAll('.timepicker');
  var instances = M.Timepicker.init(timePicker, {
    twelveHour: true,
    showClearBtn: true
  });
});

// init tabs
$(document).ready(function () {
  $('.tabs').tabs();
});

// close register-modal
function closeModal() {
  var regisModal = document.querySelector("#regis-modal");
  var regisModalInstance = M.Modal.getInstance(regisModal);
  regisModalInstance.close()
};

// tooltip handle for Name =====
function displayTooltipForName() {
  $(document).ready(function () {
    var nameTooltip = document.querySelector("#nameTooltip");
    var nameTooltipInstance = M.Tooltip.getInstance(nameTooltip);
    nameTooltipInstance.open()
  })
}

function dismissTooltipForName() {
  $(document).ready(function () {
    var nameTooltip = document.querySelector("#nameTooltip");
    var nameTooltipInstance = M.Tooltip.getInstance(nameTooltip);
    nameTooltipInstance.close()
  })
}
// end of tooltip handle for Name =====

// tooltip handle for Price =====
function displayTooltipForPrice() {
  $(document).ready(function () {
    var priceTooltip = document.querySelector("#priceTooltip");
    var priceTooltipInstance = M.Tooltip.getInstance(priceTooltip);
    priceTooltipInstance.open();
  })
}

function dismissTooltipForPrice() {
  $(document).ready(function () {
    var priceTooltip = document.querySelector("#priceTooltip");
    var priceTooltipInstance = M.Tooltip.getInstance(priceTooltip);
    priceTooltipInstance.close();
  })
}
// end of tooltip handle for Price =====

// tooltip handle for description =====
function displayTooltipForDescription() {
  $(document).ready(function () {
    var descriptionTooltip = document.querySelector("#descriptionTooltip");
    var descriptionTooltipInstance = M.Tooltip.getInstance(descriptionTooltip);
    descriptionTooltipInstance.open();
  });
}

function dismissTooltipForDescription() {
  $(document).ready(function () {
    var descriptionTooltip = document.querySelector("#descriptionTooltip");
    var descriptionTooltipInstance = M.Tooltip.getInstance(descriptionTooltip);
    descriptionTooltipInstance.close();
  });
}
// end of tooltip handle for description =====

// tooltip handle for message =====
function displayTooltipForAlternativeName() {
  $(document).ready(function () {
    var altnameTooltip = document.querySelector("#altnameTooltip");
    var altnameTooltipInstance = M.Tooltip.getInstance(altnameTooltip);
    altnameTooltipInstance.open();
  })
}

function dismissTooltipForAlternativeName() {
  $(document).ready(function () {
    var altnameTooltip = document.querySelector("#altnameTooltip");
    var altnameTooltipInstance = M.Tooltip.getInstance(altnameTooltip);
    altnameTooltipInstance.close();
  })
}
// end of tooltip handle for message =====

// get Cookies
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function selectTabUsingId(parentTab, childTab) {
  $(document).ready(function () {
    var elem = document.getElementById(parentTab);
    var tabs = M.Tabs.getInstance(elem);
    tabs.select(childTab);
  });
}

// Restricts input for the given textbox to the given inputFilter.
$(document).ready(function () {
  $('input[id="price"]').keyup(function (e) {
    if (/\D/g.test(this.value)) {
      // Filter non-digits from input value.
      this.value = this.value.replace(/\D/g, '');
    }
  });
});

$(document).ready(function () {
  switch (window.location.pathname) {
    case "/dashboard":
      $("#dashboard").addClass("active");
      break;
    case "/customer-management":
      $("#customer-management").addClass("active");
      break;
  }
});