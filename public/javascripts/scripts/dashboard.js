function callback(response) {
  productId = response;
}

// add product
function addProduct() {
  var name = $("#productName").val();
  var price = $("#price").val();
  var description = $("#description").val();
  var altName = $("#altname").val();

  if (name !== "" && price !== "" && description !== "" && altName !== "") {
    $.ajax({
      url: "/addProduct",
      type: "POST",
      dataType: "json",
      timeout: 5000,
      contentType: "application/x-www-form-urlencoded",
      headers: {
        Authorization: getCookie("token")
      },
      data: {
        name: name,
        price: price,
        description: description,
        altName: altName
      },
      beforeSend: function () {
        $("#btnAdd").prop("disabled", true); // disable button
        $(".card-panel").css("opacity", "0.5");
      },
      success: function (response) {
        $("#btnAdd").prop("disabled", false);
        $(".card-panel").css("opacity", "");

        if (response.isSuccess === false) {
          M.toast({
            html: response.message
          });
        }
        $("#hfProductId").val(response.data.productId);
        M.toast({
          html: response.data.message
        });
        selectTabUsingId("productTab", "uploadImage");
      }
    });
  } else {
    M.toast({
      html: "Must enter all information !!"
    });
  }
}

function postImageUpload(response) {
  for (i in response.data) {
    imageURL = response.data[i].originalname;
    imageUploadName = response.data[i].key;
    productId = $("#hfProductId").val();

    $.ajax({
      type: "POST",
      url: "/addImageToProduct",
      data: {
        uploadName: imageUploadName,
        imageURL: imageURL,
        productId: productId
      },
      contentType: "application/x-www-form-urlencoded",
      cache: false,
      success: function (response) {
        $("#btnUploadImg").prop("disabled", false);
        $(".card-panel").css("opacity", "");
        $("#upload-loading").css("display", "none");
        M.toast({
          html: "Product image(s) add successfully"
        });
      }
    });
  }
}

function getProducImages() {
  $("#productDefaultImage").html("");
  if ($("#hfProductId").val() === undefined || $("#hfProductId").val() === "") {
    html = `<p style="text-align: center">You must add product first !</p>`;
    html = $.parseHTML(html);
    $("#productDefaultImage").append(html);
  } else {
    $.ajax({
      type: "GET",
      url: "/getProductImages",
      data: {
        productId: $("#hfProductId").val()
      },
      cache: true,
      success: function (response) {
        images = response.data;
        if (images.length === 0) {
          html = `<p style="text-align: center">Your product not have any image(s)</p>`;
          html = $.parseHTML(html);
          $("#productDefaultImage").append(html);
        }
        images.forEach(image => {
          if (image.isHomePageImg === 1) {
            isChecked = "checked";
          } else {
            isChecked = "";
          }
          html = `<p>
            <label>
            <input class="with-gap" class="productImg" name="productImageName" id=${
              image.id
            } value=${image.id} type="radio" ${isChecked}/>
            <span style="position: absolute; color: black">${
              image.uploadName
            }</span>
            <img src="${image.imageURL}" height="20%" width="20%">
            </label>
          </p>`;
          html = $.parseHTML(html);
          $("#productDefaultImage").append(html);
        });
      },
      error: function (response) {
        M.toast({
          html: "Fetch product's image(s) failed"
        });
      }
    });
  }
}

function setDefaultImage() {
  $.ajax({
    type: "POST",
    url: "/updateProductDefaultImage",
    data: {
      productId: $("#hfProductId").val(),
      isHomePageImg: 1,
      imageId: $("input[name=productImageName]:checked").val()
    },
    cache: true,
    beforeSend: function () {
      $("#btnSetDefault").prop("disabled", true);
      $(".card-panel").css("opacity", "0.5");
    },
    success: function (response) {
      M.toast({
        html: response.data.message
      });
      $("#btnSetDefault").prop("disabled", false);
      $(".card-panel").css("opacity", "");
    },
    error: function (response) {
      M.toast({
        html: "Set product's default image failed !"
      });
      $("#btnSetDefault").prop("disabled", false);
      $(".card-panel").css("opacity", "");
    }
  });
}

function uploadImages() {
  var productId = $("#hfProductId").val();
  if (productId === undefined || productId === "" || productId === 0) {
    M.toast({
      html: "You must add product before upload image(s)"
    });
  } else if (document.getElementById("txtImgUpload").files.length == 0) {
    M.toast({
      html: "You must add file before continue !"
    });
  } else {
    var data = new FormData($("#uploadImgForm")[0]);
    data.append("productId", $("#hfProductId").val());
    $.ajax({
      type: "POST",
      url: "/uploadImage",
      data: data,
      processData: false,
      contentType: false,
      cache: false,
      beforeSend: function () {
        $("#btnUploadImg").prop("disabled", true);
        $(".card-panel").css("opacity", "0.5");
        $("#upload-loading").css("display", "");
      },
      success: function (response) {
        postImageUpload(response);
      },
      error: function (response) {
        $("#btnUploadImg").prop("disabled", false);
        $(".card-panel").css("opacity", "");
        $("#upload-loading").css("display", "none");
        M.toast({
          html: response.message
        });
      }
    });
  }
}

function getCurrentDevelopmentMode() {
  $.ajax({
    type: "GET",
    url: "/getDevelopmentModeInfo",
    beforeSend: function () {
      $(".card-panel").css("opacity", "0.5");
      document.getElementById("development-status").innerHTML = "Loading ...";
    },
    success: function (response) {
      if (response.success === true) {
        if (response.result.value === "off") {
          document.getElementById("development-status").innerHTML =
            "Development mode is off";
          $("#cbDevelopment").prop("checked", false);
        } else {
          document.getElementById("development-status").innerHTML =
            "Development mode is on";
          $("#cbDevelopment").prop("checked", true);
        }
      } else {
        document.getElementById("development-status").innerHTML =
          response.errors;
      }
      $(".card-panel").css("opacity", "");
    },
    error: function (response) {
      M.toast({
        html: "Failed to get development mode status"
      });
      $(".card-panel").css("opacity", "");
      document.getElementById("development-status").innerHTML =
        "Error when get development mode status";
      console.log(response);
    }
  });
}

function developmentModeControl() {
  var isTurnedOn = $("#cbDevelopment").prop("checked");

  if (isTurnedOn === true) {
    setting = "on";
  } else if (isTurnedOn === false) {
    setting = "off";
  }

  $.ajax({
    type: "PATCH",
    beforeSend: function () {
      $(".card-panel").css("opacity", "0.5");
      document.getElementById("development-status").innerHTML = "Loading ...";
    },
    url: "/changeDevelopmentMode",
    data: {
      data: setting
    },
    success: function () {
      getCurrentDevelopmentMode();
      M.toast({
        html: "Development setting changed successfully"
      });
    },
    error: function (response) {
      M.toast({
        html: "Failed to set development mode status"
      });
      $(".card-panel").css("opacity", "");
      document.getElementById("development-status").innerHTML =
        "Error when set development mode status";
      console.log(response);
    }
  });
}

function purgeCache() {
  $.ajax({
    type: "POST",
    beforeSend: function () {
      $(".card-panel").css("opacity", "0.5");
      document.getElementById("lblCaching").innerHTML = "Processing ...";
      $("#btnClearCache").prop("disable", true);
    },
    url: "/purgeCache",
    success: function () {
      document.getElementById("lblCaching").innerHTML = "Cache purged successfully";
      $(".card-panel").css("opacity", "");
      $("#btnClearCache").prop("disable", false);
    },
    error: function (response) {
      M.toast({
        html: "Failed to purge caching"
      });
      $(".card-panel").css("opacity", "");
      document.getElementById("development-status").innerHTML = "Cache purged failed";
      $("#btnClearCache").prop("disable", false);
      console.log(response);
    }
  });
}

function reloadGrid() {
  var loadIndicator = new jsGrid.LoadIndicator();
  loadIndicator.show();
  $("#product-grid").jsGrid("render").done(function () {
    console.log("rendering completed and data loaded");
    loadIndicator.hide();
  });
}