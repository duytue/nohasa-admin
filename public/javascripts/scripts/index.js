// login
function login() {
  var username = document.forms["login"]["txtUsername"].value;
  var password = document.forms["login"]["txtPassword"].value;
  if (username !== "" && password !== "") {

    // Login
    $.ajax({
      url: "/login",
      type: "POST",
      dataType: "json",
      data: {
        "username": $("#txtUsername").val(),
        "password": $("#txtPassword").val()
      },
      contentType: "application/x-www-form-urlencoded",
      cache: false,
      timeout: 15000,

    }).done((response) => {
      if (response.isAuthenticate === true) {
        window.location.href = "/dashboard";
      } else {
        document.getElementById("account-validate").innerHTML = "Username or password is wrong, please try again.";
      }
    });

  } else {
    if (username == "") {
      document.getElementById("username-validation").innerHTML = "Invalid username";
    }
    if (password == "") {
      document.getElementById("password-validation").innerHTML = "Invalid password";
    }
  }
}