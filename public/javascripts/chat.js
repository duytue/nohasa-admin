Talk.ready.then(function () {
    var me = new Talk.User({
        id: "1",
        name: "Admin",
        email: "admin@nohasa.com",
        photoUrl: "https://demo.talkjs.com/img/alice.jpg",
        welcomeMessage: "Hey there! How are you? :-)"
    });
    window.talkSession = new Talk.Session({
        appId: "tZSyamVL",
        me: me
    });
    var other = new Talk.User({
        id: "2",
        name: "Test User",
        email: "testuser@example.com",
        photoUrl: "https://demo.talkjs.com/img/sebastian.jpg",
        welcomeMessage: "Hey, how can I help?"
    });

    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))
    conversation.setParticipant(me);
    conversation.setParticipant(other);
    var inbox = talkSession.createInbox({
        selected: conversation
    });
    inbox.mount(document.getElementById("talkjs-container"));

    window.talkSession.unreads.on("change", function (conversationIds) {
        var amountOfUnreads = conversationIds.length;

        $(".chat-noti").text(amountOfUnreads);
    });
});