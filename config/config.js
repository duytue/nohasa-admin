const envConfig = require("./env.config");

const env = process.env.NODE_ENV || "development";

module.exports = Object.freeze({
  application: {
    API_URL: envConfig[env].API_URL,
    SECRET_KEY: envConfig[env].SECRET_KEY,
    MAX_NUMBER_FILE_UPLOAD: envConfig[env].MAX_NUMBER_FILE_UPLOAD,
    BUCKET_NAME: envConfig[env].BUCKET_NAME,
    IAM_USER_KEY: envConfig[env].IAM_USER_KEY,
    IAM_USER_SECRET: envConfig[env].IAM_USER_SECRET,
    S3_URL: envConfig[env].S3_URL,
    X_AUTH_KEY: envConfig[env].X_AUTH_KEY,
    X_AUTH_EMAIL: envConfig[env].X_AUTH_EMAIL,
    CLOUDFLARE_API_URL: envConfig[env].CLOUDFLARE_API_URL
  }
})