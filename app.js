var createError = require("http-errors");
var express = require("express");
var path = require("path");
var logger = require("morgan"); // default expressjs log
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var _ = require("lodash");
var compression = require("compression");
var helmet = require("helmet");
var minifyHTML = require("express-minify-html");
var chalk = require("chalk");
var datetime = require("./helper/datetime");
var logFile = require("./helper/logger"); // custom log file

var indexRouter = require("./routes/index");
var dashboardRouter = require("./routes/dashboard");
var customerManagement = require("./routes/customer-management");

var app = express();
app.set("trust proxy", true);
app.locals._ = _; // use lodash in view

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

var loggerFormat = "";
if (process.env.NODE_ENV === "production") {
  loggerFormat = function (tokens, req, res) {
    return [
      chalk.blue(req.ip),
      chalk.white(tokens.method(req, res)),
      tokens.url(req, res),
      chalk.green(tokens.status(req, res)),
      tokens.res(req, res, "content-length"),
      "-",
      tokens["response-time"](req, res),
      "ms",
      chalk.yellow(req.headers["user-agent"]),
      " == ",
      datetime.getDateTime()
    ].join(" ");
  };
} else {
  loggerFormat = "dev";
}

app.use(logger(loggerFormat));
app.use(express.json());
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(express.urlencoded({
  extended: false,
  limit: '50mb'
}));
app.use(cookieParser());
app.use(helmet()); // protect from vulnerabilities
app.use(compression()); // compress route for prod optimizaion
// minify HTML
app.use(
  minifyHTML({
    override: true,
    exception_url: false,
    htmlMinifier: {
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeEmptyAttributes: true,
      minifyJS: true
    }
  })
);
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter, dashboardRouter, customerManagement);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
  console.log("============ " + err + " ===============");
  logFile.error("============ " + err + " ===============");
});

module.exports = app;